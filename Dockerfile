FROM casit/php:version2.0

# Install a current version of Drush
ENV DRUSH_VERSION 8.3.5
RUN curl -L --silent https://github.com/drush-ops/drush/releases/download/${DRUSH_VERSION}/drush.phar \
  > /usr/local/bin/drush && chmod +x /usr/local/bin/drush

# Used by drush to bootstrap drupal
RUN apt-get update && apt-get install -y default-mysql-client git unzip

# For Drupal 8
RUN docker-php-ext-install opcache
COPY opcache-recommended.ini /usr/local/etc/php/conf.d
 
